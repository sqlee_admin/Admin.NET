﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Core.Util.LowCode.Front.Model
{
    public class Front_Dynamic
    {
        public string Head { get; set; }

        public string Dynamic { get; set; }

        public string DynamicKey { get; set; }
    }
}
