﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.System.LowCode.Dto
{
    public class ContrasOutput
    {
        public List<ContrastLowCode_Database> Add { get; set; }

        public List<ContrastLowCode_Database> Del { get; set; }
    }
}
